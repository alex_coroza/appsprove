/* File: gulpfile.js */

// grab our gulp packages
var gulp  = require('gulp');
var gutil = require('gulp-util');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var sass = require('gulp-sass');


gulp.task('default', function() {
	console.log('Ang pogi ko!');
});








// ADMIN MODULE FILES
gulp.task('concatMinifyModulesAdmin', function () {
	gulp.src([
		'public/modules/admin/**/*.js',
		'public/modules/core/**/*.js'
	])
	.pipe(uglify())
	.pipe(concat('public/adminModules.js'))
	.pipe(gulp.dest(''));
});


gulp.task('concatMinifyAdminVendors', function () {
	gulp.src([
		// core js files
		'public/assets/bower_components/angular/angular.js',
		'public/assets/bower_components/angular-aria/angular-aria.min.js',
		'public/assets/bower_components/angular-messages/angular-messages.min.js',
		'public/assets/bower_components/angular-animate/angular-animate.min.js',
		'public/assets/bower_components/angular-material/angular-material.js',
		'public/assets/bower_components/angular-ui-router/release/angular-ui-router.js',

		// 3rd party utility scripts
		'public/assets/bower_components/moment/moment.js',
		'public/assets/bower_components/angular-moment/angular-moment.js',
		'public/assets/bower_components/angular-busy/dist/angular-busy.js',
		'public/assets/bower_components/ng-letter-avatar/dist/ngletteravatar.min.js',
		'public/assets/bower_components/angular-timeline/dist/angular-timeline.js',
	])
	.pipe(uglify())
	.pipe(concat('public/adminVendors.js'))
	.pipe(gulp.dest(''));
});





// CUSTOMER MODULE FILES
gulp.task('concatMinifyModulesCustomer', function () {
	gulp.src([
		'public/modules/customer/**/*.js',
		'public/modules/shared/**/*.js',
		'public/modules/core/**/*.js'
	])
	.pipe(uglify())
	.pipe(concat('public/customerModules.js'))
	.pipe(gulp.dest(''));
});


gulp.task('concatMinifyCustomerVendors', function () {
	gulp.src([
		// core js files
		'public/assets/bower_components/angular/angular.js',
		'public/assets/bower_components/angular-aria/angular-aria.min.js',
		'public/assets/bower_components/angular-messages/angular-messages.min.js',
		'public/assets/bower_components/angular-animate/angular-animate.min.js',
		'public/assets/bower_components/angular-material/angular-material.js',
		'public/assets/bower_components/angular-ui-router/release/angular-ui-router.js',

		// 3rd party utility scripts
		'public/assets/bower_components/moment/moment.js',
		'public/assets/bower_components/angular-moment/angular-moment.js',
		'public/assets/bower_components/angular-busy/dist/angular-busy.js',
		'public/assets/bower_components/ng-letter-avatar/dist/ngletteravatar.min.js',
		'public/assets/bower_components/angular-timeline/dist/angular-timeline.js',
	])
	.pipe(uglify())
	.pipe(concat('public/customerVendors.js'))
	.pipe(gulp.dest(''));
});





// GUEST MODULE FILES
gulp.task('concatMinifyModulesGuest', function () {
	gulp.src([
		'public/modules/guest/**/*.js',
		'public/modules/shared/**/*.js',
		'public/modules/core/**/*.js'
	])
	.pipe(uglify())
	.pipe(concat('public/guestModules.js'))
	.pipe(gulp.dest(''));
});


gulp.task('concatMinifyGuestVendors', function () {
	gulp.src([
		// core js files
		'public/assets/bower_components/angular/angular.js',
		'public/assets/bower_components/angular-aria/angular-aria.min.js',
		'public/assets/bower_components/angular-messages/angular-messages.min.js',
		'public/assets/bower_components/angular-animate/angular-animate.min.js',
		'public/assets/bower_components/angular-material/angular-material.js',
		'public/assets/bower_components/angular-ui-router/release/angular-ui-router.js',

		// 3rd party utility scripts
		'public/assets/bower_components/moment/moment.js',
		'public/assets/bower_components/angular-moment/angular-moment.js',
		'public/assets/bower_components/angular-busy/dist/angular-busy.js',
	])
	.pipe(uglify())
	.pipe(concat('public/guestVendors.js'))
	.pipe(gulp.dest(''));
});


























// compile scss files for guest.css
gulp.task('compileGuestCss', function() {
	gulp.src(['public/assets/css/sass/guest/*.scss', 'public/assets/css/sass/core/core.scss'])
		.pipe(sass())
		.pipe(concat('guest.css'))
		.pipe(gulp.dest('public/assets/css/'));;
});




// compile scss files for customer.css
gulp.task('compileCustomerCss', function() {
	gulp.src(['public/assets/css/sass/customer/*.scss', 'public/assets/css/sass/core/core.scss'])
		.pipe(sass())
		.pipe(concat('customer.css'))
		.pipe(gulp.dest('public/assets/css/'));;
});




// compile scss files for admin.css
gulp.task('compileAdminCss', function() {
	gulp.src(['public/assets/css/sass/admin/*.scss', 'public/assets/css/sass/core/core.scss'])
		.pipe(sass())
		.pipe(concat('admin.css'))
		.pipe(gulp.dest('public/assets/css/'));;
});









// WATCH

// auto compile sass on saving
gulp.task('watchCompileScss', function() {
	gulp.watch('public/assets/css/sass/**/*.scss', ['compileGuestCss', 'compileCustomerCss', 'compileAdminCss']);
});


