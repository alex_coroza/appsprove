<!DOCTYPE html>
<html lang="en" ng-app="guestModule">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="theme-color" content="#2196F3" />

		<title>Appsprove</title>

		<link rel="stylesheet" type="text/css" href="/assets/css/customBootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/bower_components/angular-material/angular-material.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/bower_components/angular-motion/dist/angular-motion.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/bower_components/mdi/css/materialdesignicons.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/bower_components/angular-busy/dist/angular-busy.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/guest.css">
	</head>
	<body ng-cloak>
		

		<div ui-view id="main" class="am-fade-and-slide-bottom"></div>



		<!-- JS INCLUDES -->
		<!-- core js -->
		<script type="text/javascript" src="/assets/bower_components/angular/angular.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-animate/angular-animate.min.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-messages/angular-messages.min.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-aria/angular-aria.min.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-material/angular-material.min.js"></script>

		<!-- third party plugins -->
		<script type="text/javascript" src="/assets/bower_components/moment/moment.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-moment/angular-moment.min.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-busy/dist/angular-busy.min.js"></script>

		<!-- core services, filters, directives files -->
		<script type="text/javascript" src="/modules/core/services/http.service.js"></script>
		<script type="text/javascript" src="/modules/core/services/utility.service.js"></script>

		<!-- loginRegisterModule files -->
		<script type="text/javascript" src="/modules/guest/loginRegister/loginRegister.module.js"></script>
		<script type="text/javascript" src="/modules/guest/loginRegister/controllers/register.controller.js"></script>
		<script type="text/javascript" src="/modules/guest/loginRegister/controllers/login.controller.js"></script>

		<!-- guestModule base files -->
		<script type="text/javascript" src="/modules/guest/guest.module.js"></script>


		<!-- // <script type="text/javascript" src="/guestVendors.js"></script> -->
		<!-- // <script type="text/javascript" src="/guestModules.js"></script> -->

	</body>
</html>
