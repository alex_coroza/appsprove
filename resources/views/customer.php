<!DOCTYPE html>
<html lang="en" ng-app="customerModule">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="theme-color" content="#212121" />

		<title>Appsprove</title>

		<link rel="stylesheet" type="text/css" href="/assets/css/customBootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/bower_components/angular-material/angular-material.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/bower_components/angular-motion/dist/angular-motion.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/bower_components/angular-timeline/dist/angular-timeline.css">
		<link rel="stylesheet" type="text/css" href="/assets/bower_components/mdi/css/materialdesignicons.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/bower_components/angular-busy/dist/angular-busy.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/customer.css">
	</head>
	<body>
		

		<div ui-view id="main" class="am-fade-and-slide-top" cg-busy="loadingProperties"></div>



		<!-- JS INCLUDES -->
		<!-- core js -->
		<script type="text/javascript" src="/assets/bower_components/angular/angular.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-animate/angular-animate.min.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-messages/angular-messages.min.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-aria/angular-aria.min.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-material/angular-material.min.js"></script>

		<!-- third party plugins -->
		<script type="text/javascript" src="/assets/bower_components/moment/moment.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-moment/angular-moment.min.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-busy/dist/angular-busy.min.js"></script>
		<script type="text/javascript" src="/assets/bower_components/ng-letter-avatar/dist/ngletteravatar.min.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-timeline/dist/angular-timeline.js"></script>

		<!-- core services, filters, directives files -->
		<script type="text/javascript" src="/modules/core/services/http.service.js"></script>
		<script type="text/javascript" src="/modules/core/services/utility.service.js"></script>

		<!-- dashboardModule files -->
		<script type="text/javascript" src="/modules/customer/dashboard/dashboard.module.js"></script>
		<script type="text/javascript" src="/modules/customer/dashboard/controllers/dashboard.controller.js"></script>

		<!-- projectTrackerModule files -->
		<script type="text/javascript" src="/modules/customer/projectTracker/projectTracker.module.js"></script>
		<script type="text/javascript" src="/modules/customer/projectTracker/controllers/projectList.controller.js"></script>
		<script type="text/javascript" src="/modules/customer/projectTracker/controllers/projectInfo.controller.js"></script>
		<script type="text/javascript" src="/modules/customer/projectTracker/controllers/stageComments.controller.js"></script>

		<!-- guestModule base files -->
		<script type="text/javascript" src="/modules/customer/customer.module.js"></script>
		<script type="text/javascript" src="/modules/customer/customer/controllers/navbar.controller.js"></script>
		<script type="text/javascript" src="/modules/customer/customer/controllers/sidenav.controller.js"></script>



		<!-- // <script type="text/javascript" src="/customerVendors.js"></script> -->
		<!-- // <script type="text/javascript" src="/customerModules.js"></script> -->



	</body>
</html>
