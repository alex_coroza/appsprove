<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
	session_start();
	if(isset($_SESSION['loginInfo'])) {
		if($_SESSION['loginInfo']['type'] == 'Customer') return redirect('/customer');
		if($_SESSION['loginInfo']['type'] == 'Admin') return redirect('/admin');
	} else {
		return view('guest');
	}
});



Route::get('/customer/', function () {
	session_start();
	if(isset($_SESSION['loginInfo'])) {
		if($_SESSION['loginInfo']['type'] == 'Customer') return view('customer');
		if($_SESSION['loginInfo']['type'] == 'Admin') return redirect('admin');
	} else {
		return redirect('/');
	}
});



Route::get('/admin/', function () {
	session_start();
	if(isset($_SESSION['loginInfo'])) {
		if($_SESSION['loginInfo']['type'] == 'Customer') return redirect('customer');
		if($_SESSION['loginInfo']['type'] == 'Admin') return view('admin');
	} else {
		return redirect('/');
	}
});






// UserController
Route::post('user/enlist', 'UserController@enlist')->middleware('sessionCheck');
Route::post('user/register', 'UserController@register');
Route::post('user/save', 'UserController@save')->middleware('sessionCheck');
Route::post('user/resetPassword', 'UserController@resetPassword')->middleware('sessionCheck');
Route::post('user/accessCheckEmail', 'UserController@accessCheckEmail');



// ProjectController
Route::post('project/enlist', 'ProjectController@enlist')->middleware('sessionCheck');
Route::post('project/save', 'ProjectController@save')->middleware('sessionCheck');

// ProjectStageController
Route::post('projectStage/save', 'ProjectStageController@save')->middleware('sessionCheck');
Route::post('projectStage/delete', 'ProjectStageController@delete')->middleware('sessionCheck');

// ProjectStageCommentController
Route::post('projectStageComment/save', 'ProjectStageCommentController@save')->middleware('sessionCheck');



// CustomAuthController
Route::post('auth/loginInfo', 'CustomAuthController@loginInfo')->middleware('sessionCheck');
Route::post('auth/login', 'CustomAuthController@login');
Route::post('auth/logout', 'CustomAuthController@logout')->middleware('sessionCheck');
Route::post('auth/accessCheckPassword', 'CustomAuthController@accessCheckPassword');
