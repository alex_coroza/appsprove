<?php

namespace App\Traits;

use App\CustomClasses\Formatter;



trait CustomSaveTrait {


	public function customSave($param, $parentId, $fk = '') {
		$formatter = new Formatter();

		// if isang record lang
		if(is_object($param)) {
			$input = $param;
			$this->attributes = $formatter->formatModelInput($input, $this->inputRules);
			parent::save();
			$parentId = isset($this->id) ? $this->id:$parentId;
			$this->childDataSave($input, $parentId);
		} else {
			$this->where($fk, $parentId)->delete();
			foreach ($param as $key => $input) {
				$formattedInput = $formatter->formatModelInput($input, $this->inputRules);
				$formattedInput[$fk] = $parentId;
				$fkId = parent::insertGetId($formattedInput);
				$this->childDataSave($input, $fkId);
			}
		}
	}











}