<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


use App\Traits\CustomSaveTrait;



class ProjectStageComment extends Model {
	use CustomSaveTrait;


	protected $table = 'project_stage_comments';
	protected $guarded = [];

	protected $inputRules = [
		'projectStageId' => ['numericOnly'],
		'userId' => ['numericOnly'],
		'comment' => ['alphaNumericSymbolOnly']
	];




	public function projectStage() {
		return $this->belongsTo('App\ProjectStage', 'projectStageId');
	}



	public function user() {
		return $this->belongsTo('App\User', 'userId');
	}



	public function childDataSave($input, $fkId) {
		// no child data
	}








}
