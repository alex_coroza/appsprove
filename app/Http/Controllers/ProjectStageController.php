<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;


use App\ProjectStage;




class ProjectStageController extends Controller {




	public function save() {
		$options = json_decode(file_get_contents("php://input"));
		$projectStageId = isset($options->info->id) ? $options->info->id:null;

		$projectStage = ProjectStage::firstOrNew(['id' => $projectStageId]);
		$projectStage->customSave($options->info, $projectStageId);

		return $projectStage;
	}







	public function delete() {
		$options = json_decode(file_get_contents("php://input"));
		ProjectStage::destroy($options->id);
	}








	
}
