<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;


use App\ProjectStageComment;





class ProjectStageCommentController extends Controller {




	public function save() {
		$options = json_decode(file_get_contents("php://input"));
		$projectStageCommentId = isset($options->info->id) ? $options->info->id:null;

		$projectStageComment = ProjectStageComment::firstOrNew(['id' => $projectStageCommentId]);
		$projectStageComment->customSave($options->info, $projectStageCommentId);

		return $projectStageComment;
	}










}
