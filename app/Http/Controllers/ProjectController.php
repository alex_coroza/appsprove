<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;


use App\Project;




class ProjectController extends Controller {



	public function enlist() {
		$options = json_decode(file_get_contents("php://input"));
		$projectOrm = new Project();


		// conditions
		if(isset($options->conditions)) {
			foreach ($options->conditions as $condition) {
				$fieldName = $condition[0];
				$operator = isset($condition[2]) ? $condition[1] : '=';
				$value = isset($condition[2]) ? $condition[2] : $condition[1];
				$projectOrm = $projectOrm->where($fieldName, $operator, $value);
			}
		}


		// search
		if(isset($options->search)) {
			$options->search = explode(' ', $options->search);

			$projectOrm = $projectOrm->where(function($query) use($options) {
				foreach ($options->search as $key => $value) {
					$query->orWhere('name', 'LIKE', '%'.$value.'%');
					$query->orWhere('description', 'LIKE', '%'.$value.'%');
					$query->orWhere('type', 'LIKE', '%'.$value.'%');
					$query->orWhere('status', 'LIKE', '%'.$value.'%');
				}
			});
		}


		// orderBy
		if(isset($options->orderBy)) {
			foreach ($options->orderBy as $key => $value) {
				$projectOrm = $projectOrm->orderBy($value[0], $value[1]);
			}
		}


		// populate
		if(isset($options->populate)) {
			foreach ($options->populate as $populate) {
				$projectOrm = $projectOrm->with($populate);
			}
		}


		// enlist or read
		if(isset($options->id)) {
			$projectOrm = $projectOrm->where('id', $options->id)->first();
		} else {
			$projectOrm = $projectOrm->get();
		}


		return $projectOrm;
	}




	


	public function save() {
		$options = json_decode(file_get_contents("php://input"));
		$projectId = isset($options->info->id) ? $options->info->id:null;

		$project = Project::firstOrNew(['id' => $projectId]);
		$project->customSave($options->info, $projectId);

		return $project;
	}







	public function delete() {
		$options = json_decode(file_get_contents("php://input"));
		Project::destroy($options->id);
	}








}
