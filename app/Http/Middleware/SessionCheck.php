<?php

namespace App\Http\Middleware;

use Closure;



class SessionCheck {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		session_start();

		if(!isset($_SESSION['loginInfo'])) {
			return 'error';
		} else if((time() - $_SESSION['loginInfo']['lastActionTime'] > 9000)) {
			session_destroy();
			return 'error';
		} else {
			$_SESSION['loginInfo']['lastActionTime'] = time();
			return $next($request);
		}
	}
}
