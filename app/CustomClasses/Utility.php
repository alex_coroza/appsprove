<?php

namespace App\CustomClasses;



class Utility {



	// generates a random string with length based on $length
	public function generateRandomString($length = 10) {
		$characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}

		return $randomString;
	}





	public function computeAge($birthDate) {
		$date = new DateTime($birthDate);
		$now = new DateTime();
		$interval = $now->diff($date);
		return $interval->y;
	}




	// iterates an array or objects/assocArray and adds new properties(with values)
	// optionally converts the result into array or object
	public function arrayPropertyInsert($array, $propertyValues, $conversionTo = false) {
		$newArray = [];

		foreach ($array as $arrayObject) {
			foreach ($propertyValues as $key => $value) {
				if(is_object($arrayObject))$arrayObject->$key = $value;
				if(is_array($arrayObject))$arrayObject[$key] = $value;
			}

			$newArray[] = $arrayObject;
		}

		if($conversionTo == 'array') {
			$newArray = $this->objectToArray($newArray);
		} else if($conversionTo == 'object') {

		}

		return $newArray;
	}





	// converts array to object
	public function arrayToObject($array) {
		return json_decode(json_encode($array));
	}



	// converts object to array
	public function objectToArray($object) {
		return json_decode(json_encode($object), true);
	}




}

