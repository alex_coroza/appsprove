<?php


namespace App\CustomClasses;





class FileUpload {

	public $validExt = array();
	public $file;
	public $uploadPath;
	public $newFileName;


	public function __construct($file, $validExt = array('jpg', 'png', 'jpeg','JPEG', 'JPG','PNG'), $sizeLimit = '512000', $targetFolder = 'profilePhotos/') {
		$this->validExt = $validExt;
		$this->file = $file;
		$this->sizeLimit = $sizeLimit;
		// all uploaded files must be in root/app/assets/anyFolder/
		$this->uploadPath = $_SERVER['DOCUMENT_ROOT'].\URL::base().'app/assets/'.$targetFolder;
	}


	// checks if the file is lower than the size limit
	// returns true if the file has valid size
	public function checkSize() {
		if($this->file['size'] <= $this->sizeLimit)
			return true;
		else
			return false;
	}


	// checks if the file's extension is valid depending on the validExt set
	// returns true if the file has valid extension
	public function checkExtension() {
		$explodedFileName = explode('.', $this->file['name']);
		$ext = end($explodedFileName);
		if(in_array($ext, $this->validExt))
			return true;
		else
			return false;
	}


	// checks if the file has a name (meaning the upload has been cancelled if there is no filename)
	// returns true if there is a valid file name
	public function checkFileName() {
		if(strlen($this->file['name']))
			return true;
		else
			return false;
	}


	// uploads the file if it passed all the validations
	// call this method from a controller if the file passed all the validations
	public function uploadFile() {
		// give a new file name
		$explodedFileName = explode('.', $this->file['name']);
		$ext = end($explodedFileName);
		$newFileName = $this->generateRandomName().'.'.$ext;
		$this->newFileName = $newFileName;

		// move/save the file to "uploads" folder
		$tmp = $this->file['tmp_name'];
		move_uploaded_file($tmp, $this->uploadPath.$newFileName);
	}



	public function generateRandomName() {
		return rand(0, 10000000).'_'.rand(0, 10000000).'_'.rand(0, 10000000);
	}


	// returns necessary upload info/response
	public function uploadInfoResponse() {
		$response = array();
		$response['newFileName'] = $this->newFileName;
		return $response;
	}

}