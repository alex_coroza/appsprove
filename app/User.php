<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


use App\Traits\CustomSaveTrait;



class User extends Model {
	use CustomSaveTrait;


	protected $guarded = [];

	protected $inputRules = [
		'name' => ['alphaOnly', 'upperFirstWord'],
		'email' => ['emailCharsOnly'],
		'contact' => ['alphaNumericSymbolOnly'],
		'type' => ['alphaOnly', 'upperFirstWord'],
		'password' => [],
		'salt' => []
	];


	public function childDataSave($input, $fkId) {
		// no child data
	}




	
}
