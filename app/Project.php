<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


use App\Traits\CustomSaveTrait;


use App\ProjectStage;



class Project extends Model {
	use CustomSaveTrait;

	protected $guarded = [];

	protected $inputRules = [
		'customerId' => ['numericOnly'],
		'projectManagerId' => ['numericOnly'],
		'name' => ['alphaNumericSymbolOnly', 'upperFirstWord'],
		'description' => ['alphaNumericSymbolOnly'],
		'type' => ['alphaNumericSymbolOnly', 'upperFirstWord'],
		'status' => ['numericOnly'],
		'created_at' => ['datetime']
	];




	public function stages() {
		return $this->hasMany('App\ProjectStage', 'projectId');
	}



	public function customer() {
		return $this->belongsTo('App\User', 'customerId');
	}



	public function projectManager() {
		return $this->belongsTo('App\User', 'projectManagerId');
	}



	public function childDataSave($input, $fkId) {
		if(isset($input->stages)) {
			$projectStage = new ProjectStage();
			$projectStage->customSave($input->stages, $fkId, 'projectId');
		}
	}











}
