<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


use App\Traits\CustomSaveTrait;


use App\ProjectStageComment;



class ProjectStage extends Model {
	use CustomSaveTrait;


	protected $table = 'project_stages';
	protected $guarded = [];

	protected $inputRules = [
		'projectId' => ['numericOnly'],
		'note' => ['alphaNumericSymbolOnly'],
		'stage' => ['alphaOnly', 'upperFirstWord'],
		'created_at' => ['datetime']
	];




	public function comments() {
		return $this->hasMany('App\ProjectStageComment', 'projectStageId');
	}



	public function project() {
		return $this->belongsTo('App\Project', 'projectId');
	}



	public function childDataSave($input, $fkId) {
		if(isset($input->comments)) {
			$projectStageComment = new ProjectStage();
			$projectStageComment->customSave($input->comments, $fkId, 'projectStageId');
		}
	}






}
