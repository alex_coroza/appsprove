/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 10.1.16-MariaDB : Database - appsprove
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`appsprove` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `appsprove`;

/*Table structure for table `project_stage_comments` */

DROP TABLE IF EXISTS `project_stage_comments`;

CREATE TABLE `project_stage_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectStageId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `comment` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  KEY `projectStageId` (`projectStageId`),
  CONSTRAINT `project_stage_comments_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `project_stage_comments_ibfk_2` FOREIGN KEY (`projectStageId`) REFERENCES `project_stages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `project_stage_comments` */

insert  into `project_stage_comments`(`id`,`projectStageId`,`userId`,`comment`,`created_at`,`updated_at`) values (2,6,3,'lagyan nga ngatewn ng sample commnet','2016-11-26 19:42:26','2016-11-26 19:42:26'),(3,10,3,'sampel comment lang','2016-11-26 20:52:38','2016-11-26 20:52:38');

/*Table structure for table `project_stages` */

DROP TABLE IF EXISTS `project_stages`;

CREATE TABLE `project_stages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectId` int(11) DEFAULT NULL,
  `note` text,
  `stage` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `projectId` (`projectId`),
  CONSTRAINT `project_stages_ibfk_1` FOREIGN KEY (`projectId`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `project_stages` */

insert  into `project_stages`(`id`,`projectId`,`note`,`stage`,`created_at`,`updated_at`) values (6,1,'Inital data  gathering with the client.','Document Assessment','2016-11-04 19:41:47','2016-11-26 19:42:14'),(7,1,'Finalization of technical specs.','Document Assessment','2016-11-09 19:43:36','2016-11-26 19:43:54'),(8,1,'Start of development','Development','2016-11-14 19:44:00','2016-11-26 19:44:16'),(9,1,'Homepage done','Development','2016-11-16 19:44:19','2016-11-26 19:44:32'),(10,1,'Updated other pages Home, about us, services, contacts, and also added paypal services','Development','2016-11-22 19:44:34','2016-11-26 20:53:14'),(11,1,'Deployed to server','Development','2016-11-25 19:45:23','2016-11-26 19:45:40');

/*Table structure for table `projects` */

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) DEFAULT NULL,
  `projectManagerId` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `type` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `projectManagerId` (`projectManagerId`),
  KEY `customerId` (`customerId`),
  CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`customerId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `projects` */

insert  into `projects`(`id`,`customerId`,`projectManagerId`,`name`,`description`,`type`,`status`,`created_at`,`updated_at`) values (1,6,7,'Baler Spa','Website with online scheduling system','Web System',100,'2016-11-02 19:36:11','2016-11-26 19:45:54'),(2,6,3,'PJ Travel And Tours','Simple website just for viewing promos','Website',10,'2016-11-07 02:56:00','2016-11-05 09:08:06');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`contact`,`password`,`salt`,`type`,`created_at`,`updated_at`) values (3,'Alex Coroza','coroza.alex.urriza@gmail.com','09353293790','ed14ac4b2b353b7fe6c6037cdd39e33edb5a3cc8','yr7k4MwZdTbQztplgL4tRtvHt','Admin','2016-10-16 09:27:45','2016-10-16 09:27:45'),(6,'Venus Lloveras Langa','venus.chan@yahoo.com.ph',NULL,'ed14ac4b2b353b7fe6c6037cdd39e33edb5a3cc8','yr7k4MwZdTbQztplgL4tRtvHt','Customer','2016-10-25 17:32:16','2016-10-25 17:32:16'),(7,'Ronald Viray','ronald.viray@appsprove.com','09326671625','ed14ac4b2b353b7fe6c6037cdd39e33edb5a3cc8','yr7k4MwZdTbQztplgL4tRtvHt','Admin','2016-11-04 01:27:35','0000-00-00 00:00:00'),(10,'Eumii Lloveras','eumii.lloveras@gmail.com',NULL,'bd1532d693557ba8b5d16bbc87d36e58796ecce7','w5m65yccrFvaOgfCUHTDYPpOH','Customer','2016-11-04 17:02:53','2016-11-04 17:02:53');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
