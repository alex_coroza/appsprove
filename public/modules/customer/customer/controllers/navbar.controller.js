angular.module('customerModule')
.controller('navbarController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', 'httpCustomAuth', 'user', function($scope, $rootScope, $http, $state, $mdDialog, httpCustomAuth, user) {
	console.log('navbarController initialized!');


	$scope.user = user;
	$scope.pageTitle = '';

	$scope.$on('pageTitleChange', function(event, args) {
		$scope.pageTitle = args.value;
	});

	
	$scope.logout = function() {
		$http.post(httpCustomAuth.logout, {})
		.success(function(data) {
			window.location.href = '/';
		});
	};

	$scope.appsprove = function() {
		window.location.href = '//appsprove.com';
	};




	$scope.openSidenav = function() {
		$rootScope.$broadcast('openSidenav');
	};




}]);