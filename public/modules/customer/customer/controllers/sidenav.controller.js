angular.module('customerModule')
.controller('sidenavController', ['$scope', '$rootScope', '$http', '$state', '$mdSidenav', 'httpCustomAuth', 'user', function($scope, $rootScope, $http, $state, $mdSidenav, httpCustomAuth, user) {
	console.log('sidenavController initialized!');

	$scope.$on('openSidenav', function() {
		$scope.sideNavIsOpen = true;
	});

	// $scope.activeLink = 'dashboard';
	// $scope.$on('activeLinkValueUpdate', function(event, args) {
	// 	$scope.activeLink = args.value;
	// });

	
	$scope.changeState = function(state) {
		$state.go(state);
		$scope.sideNavIsOpen = false;
	};


	$scope.user = user;

	


}]);