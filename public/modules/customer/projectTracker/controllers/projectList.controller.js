angular.module('projectTrackerModule')
.controller('projectListController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', 'utilityFunctions', 'httpProject', 'user', function($scope, $rootScope, $http, $state, $mdDialog, utilityFunctions, httpProject, user) {
	console.log('projectListController initialized!');


	$rootScope.$broadcast('pageTitleChange', { value: 'Projects' });
	
	$scope.user = user;
	$scope.projects = [];
	$scope.alphabetColors = ['#F44336', '#E91E63', '#9C27B0', '#2196F3', '#009688', '#4CAF50', '#FFEB3B', '#FF5722'];

	$rootScope.loadingProperties = {};


	var loadProjects = function() {
		var options = {
			conditions: [
				['customerId', $scope.user.id]
			],
			populate: ['projectManager']
		};

		$rootScope.loadingProperties.message = 'Loading projects...';
		$rootScope.loadingProperties.promise = $http.post(httpProject.enlist, options)
		.then(function(response) {
			$scope.projects = response.data;
		});
	};



	$scope.projectInfo = function(project) {
		$state.go('customer.projectInfo', { id: project.id });
	};








	loadProjects();



}]);