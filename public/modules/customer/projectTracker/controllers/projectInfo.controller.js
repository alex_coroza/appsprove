angular.module('projectTrackerModule')
.controller('projectInfoController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', '$mdMedia', 'utilityFunctions', 'httpProject', 'user', function($scope, $rootScope, $http, $state, $mdDialog, $mdMedia, utilityFunctions, httpProject, user) {
	console.log('projectInfoController initialized!');


	$rootScope.$broadcast('pageTitleChange', { value: 'Project Info' });
	$rootScope.loadingProperties = {};


	$scope.user = user;
	$scope.project = [];
	$scope.fullscreen = $mdMedia('xs');



	var loadProject = function() {
		var options = {
			id: $state.params.id,
			conditions: [
				['customerId', $scope.user.id]
			],
			populate: ['projectManager', 'stages.comments.user'],
		};

		$rootScope.loadingProperties.message = 'Loading project info()...';
		$rootScope.loadingProperties.promise = $http.post(httpProject.enlist, options)
		.then(function(response) {
			$scope.project = response.data;
		});
	};




	$scope.showComments = function(stage) {
		$mdDialog.show({
			templateUrl: '/modules/customer/projectTracker/views/stageComments.tpl.html',
			controller: 'stageCommentsController',
			locals: {
				user: $scope.user,
				stage: stage
			},
			clickOutsideToClose: true,
			fullscreen: $scope.fullscreen
		})
		.then(function(answer) {
			// some callback action here
		});
	};






	loadProject();



}]);