angular.module('projectTrackerModule', []);



// ROUTES
angular.module('projectTrackerModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('customer.projectList', {
			url: 'projectList',
			templateUrl: '/modules/customer/projectTracker/views/projectList.view.html',
			controller: 'projectListController'
		})
		.state('customer.projectInfo', {
			url: 'projectInfo/{id}',
			templateUrl: '/modules/customer/projectTracker/views/projectInfo.view.html',
			controller: 'projectInfoController'
		})
	; /*closing $stateProvider*/
}]);
