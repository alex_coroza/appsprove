angular.module('customerModule', [
	'ngMessages', 'ngAnimate', 'ngAria', 'ui.router', 'ngMaterial',
	'angularMoment', 'cgBusy', 'ngLetterAvatar', 'angular-timeline',
	'utility.service', 'http.service',
	'dashboardModule', 'projectTrackerModule'
]);




// ROUTES
angular.module('customerModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.when('', '/projectList');
	$stateProvider
		.state('customer', {
			url: '/',
			views: {
				'': {
					templateUrl: '/modules/customer/customer/views/customer.view.html',
				},
				'navbar@customer': {
					templateUrl: '/modules/customer/customer/views/navbar.view.html',
					controller: 'navbarController'
				},
				'sidenav@customer': {
					templateUrl: '/modules/customer/customer/views/sidenav.view.html',
					controller: 'sidenavController'
				},
				'content@customer': {
					templateUrl: '/modules/customer/customer/views/content.view.html'
				}
			},
			resolve: {
				user: ['$q', '$http', 'httpCustomAuth', function($q, $http, httpCustomAuth) {
					var deferred = $q.defer();
					$http.post(httpCustomAuth.loginInfo, {})
					.success(function(data, status) {
						deferred.resolve(data)
					});
					return deferred.promise;
				}]
			}
		})
	; /*$stateProvider closing*/
}]);







// angular material date settings 
angular.module('customerModule')
.config(['$mdDateLocaleProvider', function($mdDateLocaleProvider) {
	// set format for md-datepicker
	$mdDateLocaleProvider.formatDate = function(date) {
		if(date == null) return date;
		return moment(date).format('MMM D, YYYY');
	};
}]);


// angular material theme settings
angular.module('customerModule')
.config(['$mdThemingProvider', function($mdThemingProvider) {
	$mdThemingProvider.theme('default')
		.primaryPalette('green', {
			'default': '500',
			'hue-1': '700',
			'hue-2': '900'
		})
		.accentPalette('red', {
			'default': 'A400',
			'hue-1': 'A700'
		})
}]);



// angular-busy override settings
angular.module('customerModule')
.value('cgBusyDefaults', {
	message:'Loading...',
	backdrop: true,
	// templateUrl: 'my_custom_template.html',
	delay: 0,
	minDuration: 1000,
	// wrapperClass: 'my-class my-class2'
});