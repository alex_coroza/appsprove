angular.module('dashboardModule', []);



// ROUTES
angular.module('dashboardModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('customer.dashboard', {
			url: 'dashboard',
			templateUrl: '/modules/customer/dashboard/views/dashboard.view.html',
			controller: 'dashboardController'
		})
	; /*closing $stateProvider*/
}]);