var DOMAIN = '/';

angular.module('http.service', [])


.factory('httpUser', [function() {
	this.enlist = DOMAIN+'user/enlist';
	this.register = DOMAIN+'user/register';
	this.save = DOMAIN+'user/save';
	this.resetPassword = DOMAIN+'user/resetPassword';
	this.accessCheckEmail = DOMAIN+'user/accessCheckEmail';

	return this;
}])


.factory('httpProject', [function() {
	this.enlist = DOMAIN+'project/enlist';
	this.save = DOMAIN+'project/save';
	this.delete = DOMAIN+'project/delete';

	return this;
}])


.factory('httpProjectStage', [function() {
	// this.enlist = DOMAIN+'projectStage/enlist';
	this.save = DOMAIN+'projectStage/save';
	this.delete = DOMAIN+'projectStage/delete';

	return this;
}])


.factory('httpProjectStageComment', [function() {
	this.save = DOMAIN+'projectStageComment/save';

	return this;
}])


.factory('httpCustomAuth', [function() {
	this.loginInfo = DOMAIN+'auth/loginInfo';
	this.login = DOMAIN+'auth/login';
	this.logout = DOMAIN+'auth/logout';
	this.accessCheckPassword = DOMAIN+'auth/accessCheckPassword';

	return this;
}])






; /*closing semi-colon*/
