angular.module('guestModule', [
	'ngMessages', 'ngAnimate', 'ngAria', 'ui.router', 'ngMaterial',
	'angularMoment', 'cgBusy',
	'utility.service', 'http.service',
	'loginRegisterModule'
]);




// ROUTES
angular.module('guestModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.when('', '/login');
}]);






// angular material date settings 
angular.module('guestModule')
.config(['$mdDateLocaleProvider', function($mdDateLocaleProvider) {
	// set format for md-datepicker
	$mdDateLocaleProvider.formatDate = function(date) {
		if(date == null) return date;
		return moment(date).format('MMM D, YYYY');
	};
}]);


// angular material theme settings
angular.module('guestModule')
.config(['$mdThemingProvider', function($mdThemingProvider) {
	$mdThemingProvider.theme('default')
		.primaryPalette('green', {
			'default': '700',
			'hue-2': '900'
		})
		.accentPalette('red', {
			'default': 'A400',
			'hue-1': 'A700'
		})
}]);



// angular-busy override settings
angular.module('guestModule')
.value('cgBusyDefaults', {
	message:'Loading...',
	backdrop: true,
	// templateUrl: 'my_custom_template.html',
	delay: 0,
	minDuration: 1000,
	// wrapperClass: 'my-class my-class2'
});