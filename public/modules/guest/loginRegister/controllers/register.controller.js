angular.module('loginRegisterModule')
.controller('registerController', ['$q', '$scope', '$rootScope', '$http', '$timeout', '$state', '$mdToast', '$mdDialog', 'utilityFunctions', 'formUtilities', 'regexPattern', 'httpUser', function($q, $scope, $rootScope, $http, $timeout, $state, $mdToast, $mdDialog, utilityFunctions, formUtilities, regexPattern, httpUser) {
	console.log('registerController initialized!');

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();

	$scope.loadingProperties = {};


	$scope.accountInfo = {
		type: 'Customer'
	};



	$scope.submitRegister = function(form) {
		// check for errors first
		formUtilities.formErrorChecker(form);

		// alert error message if errors detected
		// else submit the form
		if($scope.accountInfo.password != $scope.accountInfo.confirmPassword || !utilityFunctions.isEmptyObject(form.$error)) {
			var alert = $mdDialog.alert()
				.title('Errors detected!')
				.textContent('See and fix the errors to save changes!')
				.ok('Close')
			$mdDialog.show(alert);
		} else {
			console.log('success');
			$scope.loadingProperties.message = 'Checking email address...';
			$scope.loadingProperties.promise = $http.post(httpUser.accessCheckEmail, { email: $scope.accountInfo.email })
			.then(function(response) {
				if(response.data == 0) {
					$scope.loadingProperties.message = 'Submitting form...';
					return $scope.loadingProperties.promise = $http.post(httpUser.register, { info: $scope.accountInfo })
				} else {
					return $q.reject('Email address already taken!');
				}
			}).then(function(response) {
				var toast = $mdToast.simple()
					.textContent('Registration successful!')
					.position('bottom right')
				$mdToast.show(toast);
				$state.go('login');
			}).catch(function(errorMessage) {
				// jump here if any $q.reject() is found
				var alert = $mdDialog.alert()
					.title('Errors detected!')
					.textContent(errorMessage)
					.ok('Close')
				$mdDialog.show(alert);
			});
		}
	};




	$scope.checkEmail = function() {
		$http.post(httpUser.accessCheckEmail, { email: $scope.accountInfo.email })
		.then(function(response) {
			if(response.data == 0) {
				formUtilities.inputErrorToggle($scope.registerForm, 'email', 'alreadyTaken', 'errorOff');
			} else {
				formUtilities.inputErrorToggle($scope.registerForm, 'email', 'alreadyTaken', 'errorOn');
				var toast = $mdToast.simple()
					.textContent('Username already taken!')
					.position('bottom right');
				$mdToast.show(toast);
			}
		});
	};







	// ganito na lang muna, gagawan ko pa ng directive yan
	// check email availability
	var searchDelay;
	$scope.emailCheck = function() {
		if(searchDelay) $timeout.cancel(searchDelay);

		if($scope.accountInfo.email)
		searchDelay = $timeout(function() {
			$scope.checkEmail();
		}, 1500);
	};






	$scope.confirmPasswordCheck = function(form) {
		if($scope.accountInfo.password == $scope.accountInfo.confirmPassword) {
			formUtilities.inputErrorToggle(form, 'confirmPassword', 'passwordNotMatch', 'errorOff');
		} else {
			formUtilities.inputErrorToggle(form, 'confirmPassword', 'passwordNotMatch', 'errorOn');
		}
	};



	



}]);