angular.module('loginRegisterModule')
.controller('loginController', ['$scope', '$rootScope', '$q', '$http', '$state', '$mdDialog', 'utilityFunctions', 'regexPattern', 'httpCustomAuth', function($scope, $rootScope, $q, $http, $state, $mdDialog, utilityFunctions, regexPattern, httpCustomAuth) {
	console.log('loginController initialized!');

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();

	$scope.loadingProperties = {};


	$scope.email = '';
	$scope.password = '';


	$scope.login = function(form) {
		// check for errors first
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});


		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			var alert = $mdDialog.alert()
				.title('Errors detected!')
				.textContent('See and fix the errors before proceeding!')
				.ok('Close')
			$mdDialog.show(alert);
		} else {
			$scope.loadingProperties.message = 'Logging in...';
			$scope.loadingProperties.promise = $http.post(httpCustomAuth.login, { email: $scope.email, password: $scope.password })
			.then(function(response) {
				if(response.data == 'error') {
					return $q.reject('Invalid email or password!');
				} else {
					localStorage.setItem("turokJobsUserSession", JSON.stringify(response.data));
					window.location = '/customer/';
				}
			}).catch(function(errorMessage) {
				// jump here if any $q.reject() is found
				var alert = $mdDialog.alert()
					.title('Errors detected!')
					.textContent(errorMessage)
					.ok('Close')
				$mdDialog.show(alert);
			});
		}
	};




}]);