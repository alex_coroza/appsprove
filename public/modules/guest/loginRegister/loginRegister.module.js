angular.module('loginRegisterModule', []);

// ROUTES
angular.module('loginRegisterModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('register', {
			url: '/register',
			templateUrl: '/modules/guest/loginRegister/views/register.view.html',
			controller: 'registerController'
		})
		.state('login', {
			url: '/login',
			templateUrl: '/modules/guest/loginRegister/views/login.view.html',
			controller: 'loginController'
		})
	; /*$stateProvider closing*/
}]);