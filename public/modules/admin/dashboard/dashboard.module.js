angular.module('dashboardModule', []);



// ROUTES
angular.module('dashboardModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('admin.dashboard', {
			url: 'dashboard',
			templateUrl: '/modules/admin/dashboard/views/dashboard.view.html',
			controller: 'dashboardController'
		})
	; /*closing $stateProvider*/
}]);