angular.module('adminModule', [
	'ngMessages', 'ngAnimate', 'ngAria', 'ui.router', 'ngMaterial',
	'angularMoment', 'cgBusy', 'ngLetterAvatar', 'angular-timeline',
	'utility.service', 'http.service',
	'dashboardModule', 'projectTrackerModule'
]);




// ROUTES
angular.module('adminModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.when('', '/dashboard');
	$stateProvider
		.state('admin', {
			url: '/',
			views: {
				'': {
					templateUrl: '/modules/admin/admin/views/admin.view.html',
				},
				'navbar@admin': {
					templateUrl: '/modules/admin/admin/views/navbar.view.html',
					controller: 'navbarController'
				},
				'sidenav@admin': {
					templateUrl: '/modules/admin/admin/views/sidenav.view.html',
					controller: 'sidenavController'
				}
			},
			resolve: {
				user: ['$q', '$http', 'httpCustomAuth', function($q, $http, httpCustomAuth) {
					var deferred = $q.defer();
					$http.post(httpCustomAuth.loginInfo, {})
					.success(function(data, status) {
						deferred.resolve(data)
					});
					return deferred.promise;
				}]
			}
		})
	; /*$stateProvider closing*/
}]);







// angular material date settings 
angular.module('adminModule')
.config(['$mdDateLocaleProvider', function($mdDateLocaleProvider) {
	// set format for md-datepicker
	$mdDateLocaleProvider.formatDate = function(date) {
		if(date == null) return date;
		return moment(date).format('MMM D, YYYY');
	};
}]);


// angular material theme settings
angular.module('adminModule')
.config(['$mdThemingProvider', function($mdThemingProvider) {
	$mdThemingProvider.theme('default')
		.primaryPalette('green', {
			'default': '500',
			'hue-1': '700',
			'hue-2': '900'
		})
		.accentPalette('red', {
			'default': 'A400',
			'hue-1': 'A700'
		})
}]);



// angular-busy override settings
angular.module('adminModule')
.value('cgBusyDefaults', {
	message:'Loading...',
	backdrop: true,
	// templateUrl: 'my_custom_template.html',
	delay: 0,
	minDuration: 1000,
	// wrapperClass: 'my-class my-class2'
});