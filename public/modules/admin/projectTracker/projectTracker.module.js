angular.module('projectTrackerModule', []);



// ROUTES
angular.module('projectTrackerModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('admin.projectList', {
			url: 'project-list',
			templateUrl: '/modules/admin/projectTracker/views/projectList.view.html',
			controller: 'projectListController'
		})
		.state('admin.projectInfo', {
			url: 'project-info/{id}',
			templateUrl: '/modules/admin/projectTracker/views/projectInfo.view.html',
			controller: 'projectInfoController'
		})
	; /*closing $stateProvider*/
}]);
