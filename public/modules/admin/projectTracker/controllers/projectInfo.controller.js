angular.module('projectTrackerModule')
.controller('projectInfoController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', '$mdToast', '$mdMedia', 'utilityFunctions', 'httpProject', 'httpProjectStage', 'user', function($scope, $rootScope, $http, $state, $mdDialog, $mdToast, $mdMedia, utilityFunctions, httpProject, httpProjectStage, user) {
	console.log('projectInfoController initialized!');


	$rootScope.$broadcast('pageTitleChange', { value: 'Project Info' });
	$rootScope.loadingProperties = {};


	$scope.user = user;
	$scope.project = [];
	$scope.fullscreen = $mdMedia('xs');



	var loadProject = function() {
		var options = {
			id: $state.params.id,
			populate: ['projectManager', 'customer', 'stages.comments.user'],
		};

		$rootScope.loadingProperties.message = 'Loading project info...';
		$rootScope.loadingProperties.promise = $http.post(httpProject.enlist, options)
		.then(function(response) {
			$scope.project = response.data;
		});
	};




	$scope.showComments = function(stage) {
		$mdDialog.show({
			templateUrl: '/modules/customer/projectTracker/views/stageComments.tpl.html',
			controller: 'stageCommentsController',
			locals: {
				user: $scope.user,
				stage: stage
			},
			clickOutsideToClose: true,
			fullscreen: $scope.fullscreen
		})
		.then(function(answer) {
			// some callback action here
		});
	};




	$scope.projectStageEdit = function(stage) {
		$mdDialog.show({
			templateUrl: '/modules/admin/projectTracker/views/projectStageEdit.tpl.html',
			controller: 'projectStageEditController',
			locals: {
				user: $scope.user,
				projectStage: stage
			},
			clickOutsideToClose: true,
			fullscreen: $scope.fullscreen
		})
		.then(function(answer) {
			loadProject();
			var toast = $mdToast.simple()
				.textContent('Changes saved!')
				.position('bottom right');
			$mdToast.show(toast);
		});
	};




	$scope.projectStageDelete = function(stage) {
		var projectStageDeleteConfirm = confirm('Delete this stage? (this action cant be undone)');

		if(projectStageDeleteConfirm) {
			$rootScope.loadingProperties.message = 'Deleting project stage...';
			$rootScope.loadingProperties.promise = $http.post(httpProjectStage.delete, { id: stage.id })
			.then(function(response) {
				loadProject();
				var toast = $mdToast.simple()
					.textContent('Project stage deleted!')
					.position('bottom right');
				$mdToast.show(toast);
			});
		}
	};




	$scope.$on('addProjectStage', function(event, args) {
		$mdDialog.show({
			templateUrl: '/modules/admin/projectTracker/views/projectStageAdd.tpl.html',
			controller: 'projectStageAddController',
			locals: {
				user: $scope.user,
				project: $scope.project
			},
			clickOutsideToClose: true,
			fullscreen: $scope.fullscreen
		})
		.then(function(answer) {
			loadProject();
			var toast = $mdToast.simple()
				.textContent('New project stage saved!')
				.position('bottom right');
			$mdToast.show(toast);
		});
	});



	$scope.$on('editProject', function(event, args) {
		$mdDialog.show({
			templateUrl: '/modules/admin/projectTracker/views/projectEdit.tpl.html',
			controller: 'projectEditController',
			locals: {
				user: $scope.user,
				project: $scope.project
			},
			clickOutsideToClose: true,
			fullscreen: $scope.fullscreen
		})
		.then(function(answer) {
			loadProject();
			var toast = $mdToast.simple()
				.textContent('Changes saved!')
				.position('bottom right');
			$mdToast.show(toast);
		});
	});





	loadProject();



}]);