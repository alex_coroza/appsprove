angular.module('projectTrackerModule')
.controller('stageCommentsController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', 'utilityFunctions', 'httpProjectStageComment', 'user', 'stage', function($scope, $rootScope, $http, $state, $mdDialog, utilityFunctions, httpProjectStageComment, user, stage) {
	console.log('stageCommentsController initialized!');


	$scope.user = user;
	$scope.stage = stage;

	$scope.comment = '';



	$scope.postComment = function() {
		var commentInfo = {
			projectStageId: stage.id,
			userId: user.id,
			comment: $scope.comment
		};

		$http.post(httpProjectStageComment.save, { info: commentInfo })
		.then(function(response) {
			response.data.user = user; // add user para may lalabas na name
			$scope.stage.comments.push(response.data);
			$scope.comment = '';
		});
	};




	$scope.dismiss = function() {
		$mdDialog.cancel();
	};







}]);