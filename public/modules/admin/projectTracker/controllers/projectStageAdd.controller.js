angular.module('projectTrackerModule')
.controller('projectStageAddController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', 'utilityFunctions', 'regexPattern', 'httpProjectStage', 'project', 'user', function($scope, $rootScope, $http, $state, $mdDialog, utilityFunctions, regexPattern, httpProjectStage, project, user) {
	console.log('projectStageAddController initialized!');

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();
	
	$scope.user = user;
	$scope.project = project;

	$scope.projectStage = {
		projectId: project.id,
		created_at: new Date(),
		stage: 'Document Assessment'
	};



	$scope.saveProjectStage = function(form) {
		var projectStageAddConfirm = confirm('Add this stage to project?');

		// check for errors first
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});

		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			alert('See and fix the errors before proceeding!');
		} else {
			if(projectStageAddConfirm) {
				$http.post(httpProjectStage.save, { info: $scope.projectStage })
				.then(function(response) {
					$mdDialog.hide();
				});
			}
		}
	};




	$scope.dismiss = function() {
		$mdDialog.cancel();
	};






}]);