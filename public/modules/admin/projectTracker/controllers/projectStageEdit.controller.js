angular.module('projectTrackerModule')
.controller('projectStageEditController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', 'utilityFunctions', 'regexPattern', 'httpProjectStage', 'projectStage', 'user', function($scope, $rootScope, $http, $state, $mdDialog, utilityFunctions, regexPattern, httpProjectStage, projectStage, user) {
	console.log('projectStageEditController initialized!');

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();
	
	$scope.user = user;
	$scope.projectStageCopy = angular.copy(projectStage);



	$scope.saveProjectStage = function(form) {
		var projectStageAddConfirm = confirm('Add this stage to project?');

		// check for errors first
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});

		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			alert('See and fix the errors before proceeding!');
		} else {
			if(projectStageAddConfirm) {
				delete $scope.projectStageCopy.comments;
				$http.post(httpProjectStage.save, { info: $scope.projectStageCopy })
				.then(function(response) {
					$mdDialog.hide();
				});
			}
		}
	};




	$scope.dismiss = function() {
		$mdDialog.cancel();
	};






}]);