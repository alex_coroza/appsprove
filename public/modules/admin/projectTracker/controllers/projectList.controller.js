angular.module('projectTrackerModule')
.controller('projectListController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', '$mdToast', '$mdMedia', 'utilityFunctions', 'httpProject', 'user', function($scope, $rootScope, $http, $state, $mdDialog, $mdToast, $mdMedia, utilityFunctions, httpProject, user) {
	console.log('projectListController initialized!');


	$rootScope.$broadcast('pageTitleChange', { value: 'Projects' });
	$rootScope.loadingProperties = {};
	
	$scope.user = user;
	$scope.projects = [];
	$scope.alphabetColors = ['#F44336', '#E91E63', '#9C27B0', '#2196F3', '#009688', '#4CAF50', '#FFEB3B', '#FF5722'];
	$scope.fullscreen = $mdMedia('xs');



	var loadProjects = function() {
		var options = {
			populate: ['projectManager']
		};

		$rootScope.loadingProperties.message = 'Loading projects...';
		$rootScope.loadingProperties.promise = $http.post(httpProject.enlist, options)
		.then(function(response) {
			$scope.projects = response.data;
		});
	};



	$scope.projectInfo = function(project) {
		$state.go('admin.projectInfo', { id: project.id });
	};



	$scope.$on('addProject', function(event, args) {
		$mdDialog.show({
			templateUrl: '/modules/admin/projectTracker/views/projectAdd.tpl.html',
			controller: 'projectAddController',
			locals: {
				user: $scope.user
			},
			clickOutsideToClose: true,
			fullscreen: $scope.fullscreen
		})
		.then(function(answer) {
			loadProjects();
			var toast = $mdToast.simple()
				.textContent('New project saved!')
				.position('bottom right');
			$mdToast.show(toast);
		});
	});




	loadProjects();



}]);