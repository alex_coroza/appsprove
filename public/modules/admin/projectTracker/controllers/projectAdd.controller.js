angular.module('projectTrackerModule')
.controller('projectAddController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', '$mdToast', 'utilityFunctions', 'regexPattern', 'httpProject', 'httpUser', 'user', function($scope, $rootScope, $http, $state, $mdDialog, $mdToast, utilityFunctions, regexPattern, httpProject, httpUser, user) {
	console.log('projectAddController initialized!');

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();
	

	$scope.project = {
		created_at: null,
		status: 0
	};
	$scope.projectManagers = [];
	$scope.customers = [];
	

	// load all projectMangers and also admins
	var loadProjectManagers = function() {
		var options = {
			conditions: [
				['type', '!=', 'Customer']
			]
		};

		$http.post(httpUser.enlist, options)
		.then(function(response) {
			// console.log(response.data);
			$scope.projectManagers = response.data;
		});
	};




	// load all projectMangers and also admins
	var loadCustomers = function() {
		var options = {
			conditions: [
				['type', '=', 'Customer']
			]
		};

		$http.post(httpUser.enlist, options)
		.then(function(response) {
			// console.log(response.data);
			$scope.customers = response.data;
		});
	};




	$scope.saveProject = function(form) {
		var projectAddConfirm = confirm('Save this new project?');

		// check for errors first
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});

		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			alert('See and fix the errors before proceeding!');
		} else {
			if(projectAddConfirm) {
				$http.post(httpProject.save, { info: $scope.project })
				.then(function(response) {
					$mdDialog.hide();
				});
			}
		}
	};




	$scope.dismiss = function() {
		$mdDialog.cancel();
	};





	// init
	loadProjectManagers();
	loadCustomers();


}]);