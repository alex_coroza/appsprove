angular.module('adminModule')
.controller('navbarController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', 'httpCustomAuth', 'user', function($scope, $rootScope, $http, $state, $mdDialog, httpCustomAuth, user) {
	console.log('navbarController initialized!');


	$scope.user = user;
	$scope.pageTitle = '';
	$scope.currentState = $state.current.name;

	$scope.$on('pageTitleChange', function(event, args) {
		$scope.pageTitle = args.value;
	});

	
	$scope.logout = function() {
		$http.post(httpCustomAuth.logout, {})
		.success(function(data) {
			window.location.href = '/';
		});
	};




	$scope.openSidenav = function() {
		$rootScope.$broadcast('openSidenav');
	};



	// watch for state changes
	$rootScope.$on('$stateChangeStart', 
	function(event, toState, toParams, fromState, fromParams){ 
		$scope.currentState = toState.name;
	});




	// ACTIONS FOR PROJECT TRACKER MODULE
	// projectList
	$scope.projectTracker = {
		projectList: {
			addProject: function() {
				$rootScope.$broadcast('addProject');
			}
		},
		projectInfo: {
			addProjectStage: function() {
				$rootScope.$broadcast('addProjectStage');
			},
			editProject: function() {
				$rootScope.$broadcast('editProject');
			}
		}
	};








}]);